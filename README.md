# Testing Django Async

Edit docker-compose.yml to use the desired test command. Create many users (or use an existing django database with a users model).

To demonstrate an async advantage, run `python -m gunicorn simple.asgi:application -w 1 -k uvicorn.workers.UvicornWorker -b 0.0.0.0`

Open localhost:8000/async five or so times in your browser. They run numerious SQL queries. After a delay, all tabs should load.

Now try it with another command (but keep workers limited to 1). When you run the same test, you should see each tab load one by one after the same delay.

For io intensive applications that make use of async views, we expect to see better performance without needing so many web workers. One could conclude that in a production app, async Django is not faster. But it will save memory.

Database latency is simulated via tc, by default it delays requests by 3ms.

# Testing I did

![](./results.png)

10 DB users. 500 requests, 10 at a time using `ab`. WSGI requests /sync while ASGI requests /async.

| Server                                                         | Path     | AB                 | TC Delay (ms) | CPU (%) | Memory (MB) | Time Taken (s) | DB Connection  |
|---------------------------------------------------------------|----------|--------------------|---------------|---------|-------------|----------------|----------------|
| gunicorn simple.wsgi:application -w 2                         | sync     | -n 500 -c 10       | 3             | 54      | 97          | 25             | pooling        |
| gunicorn simple.wsgi:application -w 2                         | sync     | -n 500 -c 10       | 3             | 54      | 97          | 25             | CONN_MAX_AGE   |
| gunicorn simple.asgi:application -w 1 -k uvicorn.workers.UvicornWorker | async    | -n 500 -c 10       | 3             | 128     | 69          | 13             | pooling        |
| gunicorn simple.asgi:application -w 2 -k uvicorn.workers.UvicornWorker | async    | -n 500 -c 10       | 3             | 200     | 98          | 10             | pooling        |
| gunicorn simple.asgi:application -w 1 -k uvicorn.workers.UvicornWorker | async    | -n 500 -c 10       | 3             | 133     | 69          | 16             | Default        |
| gunicorn simple.wsgi:application --threads 4 -b 0.0.0.0       | sync     | -n 500 -c 10       | 3             | 81      | 69          | 12             | pooling        |
| gunicorn simple.asgi:application -w 1 --threads 2 -k uvicorn.workers.UvicornWorker | async    | -n 500 -c 10       | 3             | 129     | 67          | 14             | pooling        |
| gunicorn simple.asgi:application -w 1 -k uvicorn.workers.UvicornWorker | apsycopg | -n 500 -c 10       | 3             | 70      | 75          | 6.8            | psycopg        |


# Speculation

The "best" option looks to be wsgi with simple threading. It performs reasonably well with the lowest cpu/ram usage. I would speculate that, because Django lacks support for a fully async ORM, the gunicorn threading outperforms sync_to_async's threading. Your results may vary. One benefit of async Django is that you don't need to specify the number of threads. A real world application may at times require more ram for each gunicorn thread - which we don't see in this small test. Using a solution like fastapi, go, rust, etc may be a better option if performance is important.

I ran a direct async psycopg connection test too. As expected, it outperforms everything else. It doesn't require any threading nor any ORM operations.
