import string
import random
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = 'Generate random users'

    def add_arguments(self, parser):
        parser.add_argument('num_users', type=int, help='The number of users to create')

    def handle(self, *args, **kwargs):
        num_users = kwargs['num_users']

        for _ in range(num_users):
            username = self.generate_random_string(8)
            email = f'{username}@example.com'
            password = self.generate_random_string(12)

            User.objects.create_user(username=username, email=email, password=password)
            self.stdout.write(self.style.SUCCESS(f'User "{username}" created successfully'))

    def generate_random_string(self, length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))
