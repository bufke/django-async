from django.http import JsonResponse
from django.contrib.auth.models import User
import psycopg


repeat_count = 20


async def async_view(request):
    for _ in range(repeat_count):
        count = await User.objects.all().acount()
    usernames = [user.username async for user in User.objects.all()]
    return JsonResponse({"count": count, "usernames": usernames})


def psycopg_view(request):
    usernames = []
    with psycopg.connect("dbname=postgres user=postgres password=postgres host=postgres") as conn:
         with conn.cursor() as cur:
            for _ in range(repeat_count):
                cur.execute('SELECT COUNT(*) AS "__count" FROM "auth_user";')
                count = cur.fetchone()
            cur.execute('SELECT "auth_user"."id", "auth_user"."password", "auth_user"."last_login", "auth_user"."is_superuser", "auth_user"."username", "auth_user"."first_name", "auth_user"."last_name", "auth_user"."email", "auth_user"."is_staff", "auth_user"."is_active", "auth_user"."date_joined" FROM "auth_user"')
            usernames = [record[4] for record in cur]
    return JsonResponse({"count": count, "usernames": usernames})


async def apsycopg_view(request):
    usernames = []
    async with await psycopg.AsyncConnection.connect("dbname=postgres user=postgres password=postgres host=postgres") as conn:
         async with conn.cursor() as cur:
            for _ in range(repeat_count):
                await cur.execute('SELECT COUNT(*) AS "__count" FROM "auth_user";')
                count = await cur.fetchone()
            await cur.execute('SELECT "auth_user"."id", "auth_user"."password", "auth_user"."last_login", "auth_user"."is_superuser", "auth_user"."username", "auth_user"."first_name", "auth_user"."last_name", "auth_user"."email", "auth_user"."is_staff", "auth_user"."is_active", "auth_user"."date_joined" FROM "auth_user"')
            usernames = [record[4] async for record in cur]
    return JsonResponse({"count": count, "usernames": usernames})


def sync_view(request):
    for _ in range(repeat_count):
        count = User.objects.all().count()
    usernames = [user.username for user in User.objects.all()]
    return JsonResponse({"count": count, "usernames": usernames})