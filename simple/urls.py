from django.urls import path

from .views import async_view, sync_view, apsycopg_view, psycopg_view



urlpatterns = [
    path("async", async_view),
    path("sync", sync_view),
    path("psycopg", psycopg_view),
    path("apsycopg", apsycopg_view),
]
