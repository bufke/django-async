# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_HOME=/opt/poetry
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
WORKDIR /code
RUN curl -sSL https://install.python-poetry.org | python3 -
COPY poetry.lock pyproject.toml /code/
RUN $POETRY_HOME/bin/poetry install --no-interaction --no-ansi --no-root
COPY . /code/
